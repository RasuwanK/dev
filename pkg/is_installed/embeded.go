
package isinstalled

import _ "embed"

//go:embed "live-server.sh"
var IsLiveServerInstalled string

//go:embed "create-react-app.sh"
var IsCreateReactAppInstalled string

//go:embed "sass.sh"
var IsSassInstalled string

//go:embed "typescript.sh"
var IsTypescriptInstalled string


