# 'touch' like utility designed for devs

# About

* This utility is aimed to minimize the work that you need to do to start
  programming by creating all the additional files, installing npm modules and
  stuff like that

# Installation

* Dependencies
```
$ go
$ wget
```

* with go

```
$ go get gitlab.com/VinukaThejana/dev
```

* Build yourself

```
$ git clone https://github.com:VinukaThejana/dev.git
$ cd dev
$ go install
```

* For the installation to work smoothly go must be in your path
```
$ mkdir -p ~/go/src
$ export PATH="$PATH:$HOME/go/bin"
```
* I have only tested this project only in Linux but given the similarity between
  Linux and Unix It's safe to say that it will work on all *nix like operating
  systems


# Usage

* create files / projects

```
$ dev -f file_name/project_name.extension
```

* to list all the available file types
```
$ dev -f ls
```

* If you want to create a file(having an extension) that is not in the list
  just use the above syntax

