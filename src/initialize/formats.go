
package initialize

var Formats = map[string]bool {

	// angular-cli
	"angular-cli" : true,
	"angularCLI"  : true,

	// angular-js
	"angular-js"  : true,
	"angularJS"   : true,

	// bash
	"sh"          : true,

	// bootstrap
	"bootstrap"   : true,
	"bstrp"       : true,

	// express-js
	"expressjs"   : true,
	"express-js"  : true,
	"expressJS"		: true,
	"express"			: true,

	// go
	"go"					: true,

	// html
	"html"					: true,

	// html5
	"html5"				: true,

	// python2
	"py2"					: true,

	// python3
	"py3"					: true,

	// react-native
	"react-native": true,
	"native"			: true,
	"reactNative"	: true,
	"rn"					: true,
	"RN"					: true,

	// react
	"react"				: true,
	"r"						: true,

	// sass
	"sass"				: true,
	"scss"				: true,

	// typescript
	"ts"					: true,
	"tcs"					: true,

}

