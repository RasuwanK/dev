package identifier

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/VinukaThejana/dev/src/help"
	"gitlab.com/VinukaThejana/dev/src/initialize"
	"gitlab.com/VinukaThejana/dev/src/types"
)

func Identify() {

	// --file -f
	fileName := initialize.Init()

	// striping spaces is there is any
	fileName = strings.TrimSpace(fileName)

	// checking is user have used the flag
	// if used check wether the the flag is used in the proper way
	if fileName  == "" {
		// user have not used the flag
		// ignoring
		return
	}

	// user have used the flag
	// checking if the flag is used properly

	fileNameArray := strings.Split(fileName, ".")
	if len(fileNameArray) != 2 {
		// user have used the flag but the flag is not used properly
		if fileName == "ls" || fileName == "list"{
			fmt.Println(help.SupportedFormats)
			os.Exit(0)
		}

		fmt.Println(help.FileNameHelp)

	}

	// seprating the fileName and the ext
	ext := fileNameArray[1]

	// user have used the flag properly

	if initialize.Formats[ext] {

		switch ext {

			// angular-cli
			case "angular-cli":
				types.AngularCLI(fileName)
			case "angularCLI":
				types.AngularCLI(fileName)

			// angular-js
			case "angular-js":
				types.AngularJS(fileName)
			case "angularJS":
				types.AngularJS(fileName)

			// bash
			case "sh":
				types.Bash(fileName)

			// bootstrap
			case "bootstrap":
				types.BootStrap(fileName)
			case "bstrp":
				types.BootStrap(fileName)

			// express-js
			case "express-js":
				types.ExpressJS(fileName)
			case "ExpressJS" :
				types.ExpressJS(fileName)
			case "express":
				types.ExpressJS(fileName)
			case "expressjs":
				types.ExpressJS(fileName)

			// go
			case "go":
				types.Golang(fileName)

			// html
			case "html":
				types.Html(fileName)

			// html5
			case "html5":
				types.Html5(fileName)

			// python2
			case "py2" :
				types.Python2(fileName)

			// python3
			case "py3" :
				types.Python3(fileName)

			// react-native
			case "react-native":
				types.ReactNative(fileName)
			case "native":
				types.ReactNative(fileName)
			case "reactNative":
				types.ReactNative(fileName)
			case "rn":
				types.ReactNative(fileName)
			case "RN":
				types.ReactNative(fileName)

			// react
			case "react":
				types.React(fileName)
			case "r":
				types.React(fileName)

			// sass
			case "sass":
				types.Sass(fileName)
			case "scss":
				types.Sass(fileName)

			// typescript
			case "ts":
				types.TypeScript(fileName)
			case "tcs":
				types.TypeScript(fileName)

			default:
				// user created a file that does not exsist creating a normal file with
				// the conventional methods
				os.Create(fileName)

		}

	} else {
		// user created a file that does not exsist creating a normal file with
		// the conventional methods
		os.Create(fileName)
	}


}

