package types

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"

	"gitlab.com/VinukaThejana/dev/pkg/screen"
	"gitlab.com/VinukaThejana/dev/src/ext"
)

//html5
func Html(fileName string) {

	// Creating the html file
	os.Create(fileName)

	// checking if the file name is index.html
	if fileName == "index.html" {

		// adding the basic html boiler plate to the fileName
		file, err := os.OpenFile(fileName, os.O_APPEND, 0644)
		if err != nil {
			panic(err)
		}

		defer file.Close()

		file.WriteString("<!DOCTYPE html>\n")
		file.WriteString("<html lang='en'>\n")
		file.WriteString("    <head>\n")
		file.WriteString("        <meta charset='UTF-8'>\n")
		file.WriteString("        <meta name='viewport' content='width=device-width, initial-scale=1'>\n")
		file.WriteString("        <link href='css/style.css' rel='stylesheet'>\n")
		file.WriteString("    </head>\n")
		file.WriteString("    <body>\n")
		file.WriteString("        <h1>Hello World</h1>\n")
		file.WriteString("        <script src='js/main.js' />")
		file.WriteString("    </body>\n")
		file.WriteString("</html>\n")

		// make the javascript dir and the javascript file
		os.Mkdir("js", 0777)
		os.Create("js/main.js")

		// Identifying wether the user need sass or regular css
		fmt.Print("Do you prefer sass over css (Y/n) : ")
		var choice string
		fmt.Scanln(&choice)

		// opening the fileName in the live-server
			// check install the live-server
		ext.InstallLiveServer()

		// create a tmp file
		// add the screenliveserver script to the tmp file
		// make the tmp file executable
		// run the tmp file

		tmpFile, err := ioutil.TempFile(ext.GetCwd(), "LiveServerScreenScript")
		if err != nil {
			panic(err)
		}

		defer os.Remove(tmpFile.Name())

		tmpFile.WriteString(screen.LiveServerScreenScript)
		tmpFile.Close()

		os.Chmod(tmpFile.Name(), 0777)

		cmd := exec.Command("bash", tmpFile.Name())
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {
			fmt.Println("screen is not installed")
			fmt.Println("-----------------------")
			fmt.Println("installing screen")
			fmt.Println("-----------------------")

			ext.Installer("screen")

			cmd := exec.Command("bash", tmpFile.Name())
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			if cmd.Run() != nil {
				os.Remove(tmpFile.Name())
				panic(cmd.Run())
			}

		}

		// deciding wether user wants css or sass
		if choice == "N" || choice == "n" || choice == "No" || choice == "no" || choice == "NO" {
			// make the css dir and the style.css file in the css/
			os.Mkdir("css", 0777)
			os.Create("css/style.css")

		} else {
			// make the sass dir and the style.scss file in the sass/
			os.Mkdir("sass", 0777)
			os.Create("sass/style.scss")

			// create an empty css folder to receive the compiled sass file
			os.Mkdir("css", 0777)

			// install sass if not installed
			ext.InstallSass()

			// watching the sass folder for chnages via a screen script

			// create a tmp file
			// add the sass screen watch script to the tmp file
			// make the tmp file executable
			// run the tmp file

			tmpFile, err := ioutil.TempFile(ext.GetCwd(), "SassScreenScript")
			if err != nil {
				panic(err)
			}

			defer os.Remove(tmpFile.Name())

			tmpFile.WriteString(screen.SassScreenScript)
			tmpFile.Close()

			os.Chmod(tmpFile.Name(), 0777)

			cmd := exec.Command("bash", tmpFile.Name())
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			if err := cmd.Run(); err != nil {
				fmt.Println("screen is not installed")
				fmt.Println("-----------------------")
				fmt.Println("installing screen")
				fmt.Println("-----------------------")

				ext.Installer("screen")

				cmd := exec.Command("bash", tmpFile.Name())
				cmd.Stdout = os.Stdout
				cmd.Stderr = os.Stderr

				if cmd.Run() != nil {
					os.Remove(tmpFile.Name())
					panic(cmd.Run())
				}
			}

		}



	}
}

