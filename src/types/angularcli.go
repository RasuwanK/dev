package types

import (
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/VinukaThejana/dev/pkg/install"
	"gitlab.com/VinukaThejana/dev/src/ext"
)

func AngularCLI(projectName string) {

	// check if node is installed if not install it
	ext.SetPrefixNode()

	// seperating the project name and angular cli ext
	projectName = strings.Split(projectName, ".")[0]

	// Installing angular cli from npx
	// /usr/bin/node /usr/lib/node_modules/npm/bin/npx-cli.js --yes --package @angular/cli ng new untitled --defaults
	cmd := exec.Command("/usr/bin/node", "/usr/lib/node_modules/npm/bin/npx-cli.js", "--yes", "--package", "@angular/cli", "ng", "new", projectName, "--defaults")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		ext.SetPrefixNPM()

		cmd := exec.Command("/usr/bin/node", "/usr/lib/node_modules/npm/bin/npx-cli.js", "--yes", "--package", "@angular/cli", "ng", "new", projectName, "--defaults")
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if cmd.Run() != nil {
			panic(cmd.Run())
		}

	}

	// cd into the project
	os.Chdir(projectName)

	// installing npm packages

	// creating a temp file
	// loading the npm install script to it
	// making the tmp file executable
	// runnning the tmp file

	tmpFile, err := ioutil.TempFile(ext.GetCwd(), "NPMInstallScript")
	if err != nil {
		panic(err)
	}

	defer os.Remove(tmpFile.Name()) // clean up

	tmpFile.WriteString(install.NpmInstallScript)
	tmpFile.Close()

	os.Chmod(tmpFile.Name(), 0755)
	cmd = exec.Command("bash", tmpFile.Name())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		os.Remove(tmpFile.Name())
		panic(err)
	}

}
