package types

import (
	"strings"

	"gitlab.com/VinukaThejana/dev/pkg/zipped"
)

// BootStrap
func BootStrap(projectName string) {

	// Seperating the projectName and the extension
	projectName = strings.Split(projectName, ".")[0]

	// unzip and rename the embeded bootstrap template to the project name
	zipped.Unzip("BootStrap.zip", projectName)

}

