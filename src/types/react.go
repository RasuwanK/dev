package types

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/VinukaThejana/dev/pkg/screen"
	"gitlab.com/VinukaThejana/dev/src/ext"
)

// React
func React(projectName string) {

	// check if create-react-app is installed if not installed install it
	ext.InstallCreateReactApp()

	// seperate projectName and ext
	projectName = strings.Split(projectName, ".")[0]

	// determining wether user wont typescript in the react project or not
	fmt.Print("Do you want to use typescript in your react project? (y/N) : ")

	var choice string
	fmt.Scanln(&choice)

	// mkdir projectName
	os.Mkdir(projectName, 0777)

	// chenge the default working directory to the projectName
	os.Chdir(projectName)

	if choice == "y" || choice == "Y" || choice == "Yes" || choice == "YES" {
		// create react project with typescript
		///usr/bin/node /usr/lib/node_modules/npm/bin/npx-cli.js --yes create-react-app . --template typescript

		cmd := exec.Command("create-react-app", ".", "--template", "typescript")
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {
			ext.SetPrefixNode()
			cmd := exec.Command("create-react-app", ".", "--template", "typescript")
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			if cmd.Run() != nil {
				ext.SetPrefixNPM()
				cmd := exec.Command("create-react-app", ".", "--template", "typescript")
				cmd.Stdout = os.Stdout
				cmd.Stderr = os.Stderr

				if cmd.Run() != nil {
					panic(cmd.Run())
				}


			}

		}

	} else {
		// create react project without typescript
		// /usr/bin/node /usr/lib/node_modules/npm/bin/npx-cli.js --yes create-react-app .

		cmd := exec.Command("create-react-app", ".")
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {
			ext.SetPrefixNode()
			cmd := exec.Command("create-react-app", ".")
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			if cmd.Run() != nil {
				ext.SetPrefixNPM()
				cmd := exec.Command("create-react-app", ".")
				cmd.Stdout = os.Stdout
				cmd.Stderr = os.Stderr

				if cmd.Run() != nil {
					panic(cmd.Run())
				}


			}

		}

	}

	// opening the react project in a screen script

	// creating a tmp file
	// adding the react screen script to the tmp file
	// making the tmp file executable
	// running the tmp file

	tmpFile, err := ioutil.TempFile(ext.GetCwd(), "ReactScreenScript")
	if err != nil {
		panic(err)
	}

	defer os.Remove(tmpFile.Name())

	tmpFile.WriteString(screen.ReactScreenScript)
	tmpFile.Close()

	os.Chmod(tmpFile.Name(), 0777)

	cmd := exec.Command("bash", tmpFile.Name())

	if err := cmd.Run(); err != nil {
		fmt.Println("screen is not installed")
		fmt.Println("-----------------------")
		fmt.Println("installing screen")
		fmt.Println("-----------------------")

		cmd := exec.Command("bash", tmpFile.Name())
		if cmd.Run() != nil {
			panic(cmd.Run())
		}

	}


}

