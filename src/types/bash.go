package types

import "os"

// bash
func Bash(fileName string) {

	// creating the bash file
	// adding the bash path to the file
	// making the file executable

	os.Create(fileName)
	file, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	file.WriteString("#!/bin/bash\n")

	os.Chmod(fileName, 0755)

}

