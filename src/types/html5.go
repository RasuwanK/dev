package types

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/VinukaThejana/dev/pkg/screen"
	"gitlab.com/VinukaThejana/dev/pkg/zipped"
	"gitlab.com/VinukaThejana/dev/src/ext"
)


func Html5(projectName string) {

	// seperating the project name and ext
	projectName  = strings.Split(projectName, ".")[0]

	// create the basic html5 boilerplate (source : JetBrains WeStorm)
	zipped.Unzip("html5.zip", projectName)

	// installing the live-server
	ext.InstallLiveServer()

	// chdir to the project directory
	os.Chdir(projectName)

	// runnning the live-server on a screen session

	tmpFile, err := ioutil.TempFile(ext.GetCwd(), "LiveServerScreenScript")
	if err != nil {
		panic(err)
	}

	defer os.Remove(tmpFile.Name())

	tmpFile.WriteString(screen.LiveServerScreenScript)
	tmpFile.Close()

	os.Chmod(tmpFile.Name(), 0755)

	cmd := exec.Command("bash", tmpFile.Name())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		fmt.Println("screen is not installed")
		fmt.Println("-----------------------")
		fmt.Println("Installing screen")
		fmt.Println("-----------------------")

		ext.Installer("screen")

		cmd = exec.Command("bash", tmpFile.Name())
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {
			os.Remove(tmpFile.Name())
			panic(err)
		}

	}


}


