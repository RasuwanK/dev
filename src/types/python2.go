package types

import (
	"os"
	"os/exec"
	"strings"

	"gitlab.com/VinukaThejana/dev/src/ext"
)

//python2
func checkPython2() bool {

	cmd := exec.Command("python2", "--version")
	if err := cmd.Run(); err != nil {
		return false
	}

	return true

}


func installPython2() {

	if !checkPython2() {
		ext.Installer("python2")
	}

}


func Python2(fileName string) {

	// installling python2 if not installed
	installPython2()

	// creating the python2 file with the fileName name part
	// adding the python2 binary path to the file
	// making the file exectable

	// sepreating the filename and the extension
	fileName = strings.Split(fileName, ".")[0]
	fileName = fileName + ".py"

	os.Create(fileName)
	file, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	file.WriteString("#!/usr/bin/python2\n")
	os.Chmod(fileName, 0755)

}

